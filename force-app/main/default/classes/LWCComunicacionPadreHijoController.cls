/**
 * @file		LWCComunicacionPadreHijoController.cls
 * @author  	Ángel Troya (Accenture)
 * @date        22-06-2019
 * @description Controller class for custom app page to create a Case.  
 */
public with sharing class LWCComunicacionPadreHijoController {
    
    public LWCComunicacionPadreHijoController() {
    }

    @AuraEnabled(cacheable=false)
    public static String saveNewCase(String caseToInsert){
        System.debug('Case: ' + caseToInsert);
        Case caseObject = (Case)JSON.deserialize(caseToInsert, Case.class);

        Database.SaveResult[] caseResult;
        caseResult = Database.insert(new List<Case>{caseObject}, false);
        System.debug('caseResult: ' + caseResult);

        System.debug('result: ' + caseResult);
		DmlResultWrapper resultWrap = new DmlResultWrapper(caseResult[0]);
		
        System.debug('Resultado: '+resultWrap);
		return JSON.serialize(resultWrap);
    }

    public class DmlResultWrapper {
		public Boolean success {get;set;}
		public Id recordId {get;set;}
		public String caseNumber {get;set;}
		public Database.Error[] errors {get;set;}
		public DmlResultWrapper(Database.SaveResult saveResult) {
			List<Case> cases; 
			this.success = saveResult.isSuccess();
			this.recordId = saveResult.getId();
			cases = [SELECT CaseNumber FROM Case WHERE Id= :this.recordId];
			if(!cases.isEmpty()){
				this.caseNumber= cases[0].caseNumber;
			}
			System.debug('caseNumber: ' + caseNumber);
			this.errors = saveResult.getErrors();
		}
	}
}
