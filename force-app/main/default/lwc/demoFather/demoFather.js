import { LightningElement, track } from 'lwc';

import saveNewCase from '@salesforce/apex/LWCComunicacionPadreHijoController.saveNewCase';
import { NavigationMixin } from 'lightning/navigation';

export default class DemoFather extends NavigationMixin(LightningElement) {

	@track
	myCase = {
		case: {}
		//case: { recordTypeId: '0126E000000R7wAQAS' }
	};

	/* Custom Toast Variables */
	@track
	toastTitle = '';
	@track
	toastMessage = '';
	@track
	toastVariant = 'error';
	@track
	toastAutoClose = false;
	
	@track //update this property in the template
	showSpinner = false;
	
	//Cancel button execute
	previouStepHandler(event) {
		event.preventDefault();
		window.open('/lightning/o/Case/list', "_self");
	}

	//Save button execute
	handleClickValidate(event){
		var customToast = this.template.querySelector('c-lwc-custom-toast');
		customToast.hideCustomNotice();
		this.showSpinner = true;
		//Execute validateFields method 
		this.template.querySelector('c-demo-first-child').validateFields();
	}

	submitDataFromChild_Step1(event) {
		var prop;
		var obj = JSON.parse(event.detail);
		event.preventDefault();
		for (prop in obj.case.fields) {
			//Good practices
			if (prop) {
				this.myCase.case[prop] = obj.case.fields[prop];
			}
		}
		this.handleValidate2(event);
	}

	handleValidate2(event) {
		event.preventDefault();
		window.console.log('Going to Child 2...');
		this.template.querySelector('c-demo-second-child').validateFields();
	}

	submitDataFromChild_Step2(event) {
		var prop;
		var obj = JSON.parse(event.detail);
		event.preventDefault();
		for (prop in obj.case.fields) {
			//Good practices
			if (prop) {
				this.myCase.case[prop] = obj.case.fields[prop];
			}
		}
		this.handleValidate3(event);
	}

	handleValidate3(event) {
		event.preventDefault();
		window.console.log('Going to Child 3...');
		this.template.querySelector('c-demo-third-child').validateFields();
	}

	submitDataFromChild_Step3(event) {
		var prop;
		var obj = JSON.parse(event.detail);
		event.preventDefault();
		for (prop in obj.case.fields) {
			//Good practices
			if (prop) {
				this.myCase.case[prop] = obj.case.fields[prop];
			}
		}
		
		saveNewCase({ caseToInsert: JSON.stringify(this.myCase.case) })
			.then(resultString => {
				//Select custom toast to modify it depeding on result
				var customToast = this.template.querySelector('c-lwc-custom-toast');

				this.showSpinner = false;

				const result = JSON.parse(resultString);
				if (result.success) {
					this.toastTitle = '¡Éxito';
					this.toastMessage = 'La solicitud se ha insertado correctamente con el número: ' + result.caseNumber;
					this.toastVariant = 'success';
					this.toastAutoClose = true;
					customToast.showCustomNotice();

					// eslint-disable-next-line @lwc/lwc/no-async-operation
					setTimeout(() => {
						this[NavigationMixin.Navigate]({
							type: 'standard__recordPage',
							attributes: {
								objectApiName: this.type,
								actionName: 'view',
								recordId: result.recordId
							}
						})
					}, 3000);
				}
				else {
					var errors = '';

					for (var idx in result.errors) {
						if (result.errors[idx].message)
							errors += result.errors[idx].message + '\n';
					}

					this.toastTitle = 'Por favor, antes de continuar revise los siguientes errores: ';
					this.toastMessage = errors;
					this.toastVariant = 'error';
					this.toastAutoClose = false;
					customToast.showCustomNotice();
				}
			})
			.catch(error => {

				this.toastTitle = 'Se ha producido un error. Si fuera necesario, por favor, avise a su administrador: ';
				this.toastMessage = error;
				this.toastVariant = 'error';
				this.toastAutoClose = false;
				customToast.showCustomNotice();

			});
	}

	handleUpdateCustomToast(event) {
		var jsonObj = JSON.parse(event.detail);
		var customToast = this.template.querySelector('c-lwc-custom-toast');

		window.console.log('Updating custom toast...');
		this.toastTitle = jsonObj.toastTitle;
		this.toastMessage = jsonObj.toastMessage;
		this.toastVariant = jsonObj.toastVariant;
		this.toastAutoClose = jsonObj.toastAutoClose;
		customToast.showCustomNotice();
		this.showSpinner = false;
	}
}