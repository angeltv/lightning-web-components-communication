import { LightningElement, api } from 'lwc';

export default class DemoFirstChild extends LightningElement {

	constructor() {
		super();
		if (!this.case) {
			this.myCase = { case: {} };
		}
	}

	//executing from father, make click in hidden submit button in Child 1
	@api
	validateFields() {
		var submitBtn = Array.from(this.template.querySelectorAll('lightning-button'))
			.filter(element => element.title === "save_CaseChild_1")[0];
		window.console.log("Validating First Child...");

		submitBtn.click();
	}

	saveCaseFirstChild(event) {
		window.console.log("Submit button in first child clicked, saving First Child...");
		event.preventDefault();
		const fields = event.detail.fields;
		this.myCase.case.fields = fields;

		//Send case to father
		const evt = new CustomEvent('submitdataone', { detail: JSON.stringify(this.myCase) });
		this.dispatchEvent(evt);
	}
}